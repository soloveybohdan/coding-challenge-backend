## About solution
It was my very first experience with flask and mongo, so maybe its not the best style.
I choose `mongoDB` as db, because we have only 1 model, without relations =>
non-relation db is good for this case.
As framework i choose `Flask`, because this project is simple, 
and we dont need all features that `Django` gives out-of-the-box. 
## About project
This is a simple app that gives only 1 endpoint for `GET` method.
All data from github downloads at first start (for more: `create_app()`), 
its takes some time, but there is no task for data importing endpoint.
## Bonus 1
example usage: `http://0.0.0.0:5000/repositories/?page=2&sort=star` or `sort=-star`.

## Bonus 2
$ docker-compose up

### Make Initial Setup:
$ git clone https://soloveybohdan@bitbucket.org/soloveybohdan/coding-challenge-backend.git

$ cd coding-challenge-backend

## Developed at :
Ubuntu 18.04.3 LTS

Docker version 18.09.7

docker-compose version 1.23.1

Python 3.6 (but in container 3.7).
