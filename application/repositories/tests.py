import unittest
from mockupdb import MockupDB

from application import create_app

TEST_DB = 'test.db'


class RepositoriesTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = MockupDB(auto_ismaster=True, verbose=True)
        cls.server.run()
        # but im not sure_)
        # create mongo connection to mock server
        app = create_app()
        app.config['MONGO_URI'] = cls.server.uri
        cls.app = app.test_client()

    @classmethod
    def tearDownClass(cls):
        cls.server.stop()

    def test_repos_list(self):
        response = self.app.get('/repositories', follow_redirects=True)
        self.assertEqual(
            response.status_code, 200, 'Can not get list of repos'
        )
        self.assertEqual(
            len(response.json['items']), 10, 'Bad count of items'
        )

    def test_pagination(self):
        response = self.app.get(
            '/repositories' + '?page=1', follow_redirects=True
        )
        self.assertEqual(
            response.status_code, 200, 'Can not get list of repos'
        )
        self.assertEqual(response.json['next_page'], 2, 'Bad pagination_data')
        old_items = response.json['items']
        response = self.app.get(
            '/repositories' + '?page=2', follow_redirects=True
        )
        self.assertEqual(
            response.status_code, 200, 'Can not get list of repos'
        )
        self.assertEqual(response.json['prev_page'], 1, 'Bad pagination_data')
        self.assertNotEqual(
            old_items, response.json['items'][0],
            'Items on different pages are the same'
        )

    def test_star_sorting(self):
        response = self.app.get(
            '/repositories' + '?sort=star', follow_redirects=True
        )
        self.assertEqual(
            response.status_code, 200, 'Can not get list of repos'
        )
        self.assertTrue(
            response.json['items'][0]['stargazers_count'] <=
            response.json['items'][1]['stargazers_count'],
            'Bad sorting from low to high'
        )
        response = self.app.get(
            '/repositories' + '?sort=-star', follow_redirects=True
        )
        self.assertEqual(
            response.status_code, 200, 'Can not get list of repos'
        )
        self.assertTrue(
            response.json['items'][0]['stargazers_count'] >=
            response.json['items'][1]['stargazers_count'],
            'Bad reverse sorting'
        )


if __name__ == "__main__":
    unittest.main()
