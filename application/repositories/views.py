import json

from flask import Response
from flask_restful import Resource, reqparse

from application.database.models import Repo


class RepoListView(Resource):
    """
    View that returns list of repositories.
    """
    DEFAULT_NUMBER_OF_RESULTS = 10

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'page', type=int, default=1, required=False,
        )
        self.reqparse.add_argument(
            'sort', type=str, default='star', required=False,
        )
        super(RepoListView, self).__init__()

    def get(self):
        args = self.reqparse.parse_args()  # sorting_params, page_number
        page = self.get_paginated_data(
            args, self.DEFAULT_NUMBER_OF_RESULTS,
        )
        # we need to serialize data
        return Response(
           json.dumps(page), mimetype="application/json", status=200
        )

    def get_sorting_param(self, args):
        """
        For geting query_params and transform it to params,
        that can be used for SQL ORDER_BY
        """
        sorting_param = args.get('sort').lower()
        acceptable_params = ['star']
        if sorting_param.startswith('-') \
                and sorting_param[1:] in acceptable_params \
                or sorting_param in acceptable_params:
            if sorting_param in ('star', '-star'):
                # sign=' ' for sort low to high or '-' reverse
                order_sign = '' if sorting_param == 'star' else '-'
                sorting_param = '{order_sign}stargazers_count'.format(
                    order_sign=order_sign
                )
        return sorting_param

    def get_paginated_data(self, args, per_page):
        """
        This method used for paginating and sorting
        :param args: current_page
        :param per_page: number of results
        :return: paginated results in readable format
        """
        field = self.get_sorting_param(args)
        repos = Repo.objects.order_by(field).paginate(
            page=args['page'], per_page=per_page
        )
        items = [  # more user-friendly format that to_json
            {
                "full_name": item.full_name,
                "html_url": item.html_url,
                "description": item.description,
                "stargazers_count": item.stargazers_count,
                "language": item.language,
            }
            for item in repos.items
        ]
        # information of current pagination session
        results = {
            'current_page': repos.page,
            'next_page': repos.page + 1,
            'prev_page': repos.page - 1,
            'all pages': repos.pages,
            'items on page': repos.per_page,
            'total': repos.total,  # total number of items that matches query
            'items': items
        }
        return results
