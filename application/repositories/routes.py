from application.repositories.views import RepoListView


def initialize_routes(api):
    api.add_resource(RepoListView, '/repositories/')
