import json
import logging
import time

import requests
from requests.auth import HTTPBasicAuth

logger = logging.getLogger(__name__)


class GitHubSearch(object):
    """
    For work with github
    """

    def __init__(self, user=None, password=None):
        self.api = 'https://api.github.com/search/repositories?' \
              'q=language:{}+stars:>={}&order=desc&page={}&per_page=100'
        # in our case we should use ?q=language:python+stars:>=500
        self.fields = (
            'full_name', 'html_url', 'description', 'stargazers_count',
            'language'
        )
        self.user = user
        self.password = password

    def get_page(self, page, language='python', min_stars=500):
        """
        Method for make a single request.
        :param page: position
        :param language: programming language of repo
        :param min_stars: minimal number of stars that we need
        :return: data from 1 page (100 items)
        """
        url = self.api.format(language, min_stars, page)
        auth = HTTPBasicAuth(self.user, self.password)\
            if self.user is not None or self.password is not None else None
        response = requests.get(
            url, auth=auth
        )
        while response.status_code == 403:  # omit github searches limit
            logging.info('cannot connect to github -- retry in 20 ')
            time.sleep(20)
            response = requests.get(
                url, auth=auth
            )
        return response.content

    def clean_data(self, item):
        # returns only fields from self.fields
        return {field: item[field] for field in item if field in self.fields}

    def get_parsed_info(self):
        """
        Parse data from github and return list of results.
        """
        result = []
        logger.info('---data getting---')
        for i in range(1, 11):
            #  we have  ~ 5k results, but github says:
            # "Only the first 1000 search results are available"
            full_page_data = self.get_page(i, 'python', 500)
            if full_page_data is not None:
                full_page_data = json.loads(full_page_data)
            items = full_page_data.get('items')
            result += [self.clean_data(item) for item in items]
        logger.info('---end of data getting---')
        return result


def init_github_data(user=None, password=None):
    """
    Get data from github, parse it and add to list.
    :param user: for auth
    :param password: for auth
    :return:
    """
    instance = GitHubSearch(user=user, password=password)
    results = instance.get_parsed_info()
    return results


def save_initial_data(data):
    """
    Uploading data from list to DB.
    """
    from application.database.models import Repo
    for instance in data:
        Repo(**instance).save()
