from flask_mongoengine import Document
from mongoengine import StringField, IntField


class Repo(Document):
    """
    Model for storing repositories in DB
    """
    full_name = StringField(required=True, unique=False)
    html_url = StringField(required=True, unique=True)
    description = StringField(required=False, unique=False)
    stargazers_count = IntField(required=True, unique=False)
    language = StringField(required=True, unique=False)
