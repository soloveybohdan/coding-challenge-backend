import os

from flask import Flask
from flask_restful import Api
from mongoengine import NotUniqueError

from application.database.db import initialize_db
from application.repositories.github import init_github_data, save_initial_data
from application.repositories.routes import initialize_routes


def create_app():
    """Creates a new Flask application and initialize application."""
    app = Flask(__name__)
    api = Api(app)
    db_host = os.environ.get('MONGO_HOST', 'localhost')
    db_name = os.environ.get("MONGO_DB_NAME", 'mydatabase')
    db_port = os.environ.get("MONGO_PORT", '27017')
    app.config['MONGODB_SETTINGS'] = {'db': 'testing', 'alias': 'default'}
    app.config['MONGODB_SETTINGS'] = {
        'host': 'mongodb://{host}:{port}/{db}'.format(
            host=db_host, port=db_port, db=db_name
        ),
    }

    initialize_db(app)
    user = os.environ.get('GITHUB_USER')
    password = os.environ.get("GITHUB_PASSWORD")
    try:
        data = init_github_data(user, password)
        save_initial_data(data)
    except NotUniqueError:
        pass
    initialize_routes(api)
    return app
