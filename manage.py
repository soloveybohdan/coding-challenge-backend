import os

from application import create_app

app = create_app()

if __name__ == "__main__":
    app.run(
        host='0.0.0.0',  debug=os.environ.get('DEBUG', True),
        use_reloader=False
    )