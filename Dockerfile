FROM python:3.7

RUN mkdir /app

COPY ./requirements.txt /app/requirements.txt

RUN apt-get update
RUN pip3 install -r /app/requirements.txt
COPY ./ /app/


WORKDIR /app
EXPOSE 5000
VOLUME ["/app/data"]

CMD python manage.py runserver
